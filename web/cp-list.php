<?php
    session_start();
    if(!isset($_SESSION['rol']))
    {
        header('Location:login.php');
    }
    else{
        include('database.php');
        $records = $connection->prepare('SELECT * FROM control_points;');
        $records->execute();
        $control_points = $records->fetchAll();
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control de acceso FI UAEMex</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/litera/bootstrap.min.css"></link>
</head>
<body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="">
          <img src="img/uaemex_logo.png" alt="UAEMex">
          UAEMex
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor03">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Caracteristicas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Acerca de</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=$_SESSION['username']?></a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Configuracion de la cuenta</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logout.php">Cerrar sesion</a>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto">
          </ul>
        </div>
    </nav>
    <!--Navbar-->

    <div class="container px-0 mx-0">
        <div class="row">
            <div class="col-md-3 bg-light d-flex flex-column">
              <div class="mt-4 d-flex align-items-center justify-content-center">
                <!--<h4 class=""><?=$_SESSION['rol']?></h4><br>-->
              </div>      
              <div class="mt-4 d-flex align-items-center justify-content-center">
                <a href="cp-list.php"><strong>Puntos de control</strong></a>
              </div>
              <div class="mt-4 d-flex align-items-center justify-content-center">
                <a href="registro-accesos.php">Registro de accesos</a>
              </div>
              <!--
              <div class="mt-4 d-flex align-items-center justify-content-center">
                <a href="registro-accesos.php">Registrar a una persona</a>
              </div>
              -->
            </div>
            <div class="col-md-9">
                <h3 class="mt-4 mb-4 ml-4">Puntos de control</h3>
                <div class="mb-4 d-flex justify-content-end">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#newModal">NUEVO</button>
                </div>
                <table class="table table-hover ml-4 pl-4">
                    <thead>
                        <tr class="table-active">
                          <th scope="col">CLAVE</th>
                          <th scope="col">FACULTAD</th>
                          <th scope="col">DESCRIPCION</th>
                          <th scope="col"></th>
                          <th scope="col">ACCIONES</th>
                          <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                        <?php for ($i = 0; $i < sizeof($control_points); $i++){?>
                            <tr>
                                <td scope="row"><?= $control_points[$i]['id_control_point']?></td>
                                <td><?= $control_points[$i]['facultad']?></td>
                                <td><?= $control_points[$i]['descripcion']?></td>
                                <td>
                                <a class="btn btn-success" href="punto-control.php?id_cp=<?=$control_points[$i]['id_control_point']?>">VER</a>
                                </td>
                                <td>
                                <button class="edit-btn btn btn-primary" id="">EDITAR</a>
                                </td>
                                <td>
                                <button class="delete-btn btn btn-danger" id="">ELIMINAR</a>
                                </td>
                            </tr>
                        <?php };?>
                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    
<div class="modal fade" id="newModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear nuevo punto de control</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Facultad:</label>
            <select class="custom-select" id="cp_facultad">
              <option value="Facultad de Ingeniería">Facultad de Ingeniería</option>
            </select>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Descripcion:</label>
            <textarea class="form-control" id="cp_descripcion"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel-btn">Cancelar</button>
        <button type="button" class="btn btn-primary" id="create-btn">Crear</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar punto de control</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Facultad:</label>
            <select class="custom-select" id="edit_facultad">
              <option value="Facultad de Ingeniería">Facultad de Ingeniería</option>
            </select>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Descripcion:</label>
            <textarea class="form-control" id="edit_descripcion" value=""></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel-btn">Cancelar</button>
        <button type="button" class="btn btn-primary" id="edit-btn">Crear</button>
      </div>
    </div>
  </div>
</div>
    

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="js/cp-list-func.js"></script>
</body>
</html>