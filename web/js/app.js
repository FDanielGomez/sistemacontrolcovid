$(document).ready( function () {

    let id_cp = $('#id_cp').html();
    $.ajax({
        type:"POST",
        url:"qrGenerator.php",
        data: { id_cp },
        success: function (data) {
            $('#qr_container').html('<img class=\"mx-auto\" src=\"data:image/png;base64,' + data + '\">');
        }
    });  
});