<?php
    include('database.php');
    $id_cp = $_POST['id_cp'];
    $records = $connection->prepare('DELETE FROM control_points WHERE id_control_point = :id_cp');
    $records->bindParam('id_cp',$id_cp);
    if($records->execute()){
        $records = $connection->prepare('SELECT id_control_point,descripcion,facultad FROM control_points;');
        $records->execute();
        $result = $records->fetchAll();
        echo json_encode($result);
    }
?>